# Firmware
PRODUCT_COPY_FILES += \
    vendor/lenovo-firmware/zippo/aop.mbn:install/firmware-update/aop.mbn \
    vendor/lenovo-firmware/zippo/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/lenovo-firmware/zippo/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/lenovo-firmware/zippo/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/lenovo-firmware/zippo/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/lenovo-firmware/zippo/dspso.bin:install/firmware-update/dspso.bin \
    vendor/lenovo-firmware/zippo/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/lenovo-firmware/zippo/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/lenovo-firmware/zippo/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    vendor/lenovo-firmware/zippo/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/lenovo-firmware/zippo/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/lenovo-firmware/zippo/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/lenovo-firmware/zippo/tz.mbn:install/firmware-update/tz.mbn \
    vendor/lenovo-firmware/zippo/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/lenovo-firmware/zippo/xbl.elf:install/firmware-update/xbl.elf
